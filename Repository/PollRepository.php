<?php

namespace ATM\PollBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PollRepository extends EntityRepository{

    public function getForbiddenDates($pollId = null)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('p.init_date,p.end_date')
            ->from('ATMPollBundle:Poll','p');

        if(!is_null($pollId)){
            $qb->where(
                $qb->expr()->neq('p.id',$pollId)
            );
        }

        $aDates = array();

        foreach($qb->getQuery()->getArrayResult() as $poll){
            $initDate = $poll['init_date'];
            $endDate = $poll['end_date'];
            while($initDate->format('dmY') != $endDate->format('dmY')){
                $aDates[] = $initDate->format('d-m-Y');
                $initDate->modify('+1 day');
            }
            $aDates[] = $endDate->format('d-m-Y');
        }

        return $aDates;
    }
}